# Ansible Role: Lens

This role installs [Lens](https://k8slens.dev/) binary on any supported host.

## Requirements

None

## Installing

The role can be installed by running the following command:

```bash
git clone https://gitlab.com/enmanuelmoreira/ansible-role-lens enmanuelmoreira.lens
```

Add the following line into your `ansible.cfg` file:

```bash
[defaults]
role_path = ../
```

## Role Variables

Available variables are listed below, along with default values (see `defaults/main.yml`):

    lens_version: 5.3.0
    lens_date_build: 20211125
    lens_bin_path: /usr/bin/lens

This role always installs the desired version. See [available Lens releases](https://k8slens.dev/) also on [GitHub](https://github.com/lensapp/lens/releases/).

    lens_version: 5.3.0

The version to the Lens application.

    lens_date_build: 20211125

The date of the version build.

    lens_package_name: lens

The name of the package installed in the system (if there need to be updated it).

## Dependencies

None.

## Example Playbook

    - hosts: all
      become: yes
      roles:
        - role: enmanuelmoreira.lens

## License

MIT / BSD
